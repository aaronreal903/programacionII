import java.awt.*;
import javax.swing.*;

public class ComponentesAwt{
	JFrame f;
	JPanel p1;
	JLabel e, image;
	JTextField tf;
	TextArea ta;
	Choice ch;
	Font fuente = new Font("Lucida calligraphy", Font.BOLD, 20);
	Icon img = new ImageIcon(getClass().getResource("descarga.png"));
	MenuBar mb;
	Menu m1, m2;
	MenuItem mi1, mi2, mi3;
	Checkbox cb, cb2;
	CheckboxGroup cg;
	JTabbedPane pestanas;

	public ComponentesAwt(){

	f = new JFrame("AWT Components");
	p1 = new JPanel();
	p1.setBackground(Color.GREEN);
	image = new JLabel();
	//f.add(image);
	image.setIcon(img);
	p1.add(image);
	image.setText("QUE LE VALGA VERGA MI DYN");
	image.setHorizontalTextPosition(SwingConstants.CENTER);
	image.setVerticalTextPosition(SwingConstants.BOTTOM);
	image.setFont(fuente);
	image.setForeground(Color.BLACK);
	ta = new TextArea();
	pestanas = new JTabbedPane();
	pestanas.addTab("Nuevos", p1);
	pestanas.addTab("Viejos", ta);
	//e = new JLabel("Hello world");
	//e.setFont(fuente);
	/*tf = new JTextField("Enter your name",25);
	tf.setFont(fuente);
	ta =  new TextArea("", 10, 10, TextArea.SCROLLBARS_NONE);
	ch =  new Choice();
	for(int i = 1; i<=10; i++)
		ch.add(String.valueOf(i));
	cg = new CheckboxGroup();
	cb = new Checkbox("Masculino", false, cg);
	cb2 = new Checkbox("Femenino", false, cg);

	mb = new MenuBar();
	m1 =  new Menu("Archivo");
	m2 = new Menu("Edicion");
	mi1 =  new MenuItem("Nuevo");
	mi2 =  new MenuItem("Guardar");
	mi3 = new MenuItem("Ayuda"); */

	/*pestanas = new JTabbedPane();
	pestanas.addTab("Nuevos");
	pestanas.addTab("Viejos");

	mb.add(m1);
	mb.add(m2);
	m1.add(m2);
	m1.addSeparator();
	m1.add(mi2);
	m2.add(mi3);

	f.setMenuBar(mb);
	//mb.setBackground(Color.YELLOW);
	f.add(cb);
	f.add(cb2);
	ta.setBackground(Color.GREEN);
	ta.setForeground(Color.BLACK);
	tf.setForeground(Color.BLACK);
	tf.setBackground(Color.GREEN);
	cb.setBackground(Color.BLUE);
	cb.setForeground(Color.WHITE);
	cb2.setBackground(Color.PINK);
	f.setLayout(new FlowLayout());
	//f.add(e);
	f.add(tf);
	f.add(ta);
	f.add(ch);

	*/
	f.add(pestanas, BorderLayout.CENTER);
	f.setSize(600,450);
	f.setLocation(500, 250);
	f.setVisible(true);
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	f.getContentPane().setBackground(Color.BLACK);

}

	public static void main(String[]args){
		new ComponentesAwt();

	}
}