import java.io.*;
import java.util.*;

public class LeeTipos{
	public static void main(String[]args){
		FileInputStream fis = null;
		DataInputStream dis = null;
try{
fis=new FileInputStream("datos.dat");
dis=new DataInputStream(fis);

BufferedReader br=new BufferedReader(new InputStreamReader(fis));
System.out.println("Entero: "+dis.readInt());
System.out.println("Flotante: "+dis.readDouble());
System.out.println("Cadena: "+br.readLine());

fis.close();
dis.close();
}catch (IOException E){
System.out.print("Error...");
E.printStackTrace();

}
}
}