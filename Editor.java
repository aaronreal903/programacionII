import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;


public class Editor implements ActionListener, ItemListener {
	JFrame f = new JFrame();
	Font letra = new Font("Arial", Font.BOLD, 16);
	Font letra2 = new Font("Courier", Font.BOLD, 14);
	Font letra3 = new Font("Lucida Calligraphy", Font.BOLD, 14);
	JTabbedPane pestana;
	JTextArea texto[];
	MenuBar menu;
	Menu archivo, edicion;
	MenuItem nuevo, guardar, guardar2, abrir, copiar, cortar, pegar, mayus, minus; 
	JToolBar tool, tool2;
	String txt, ruta, buttonTitle[] = {"COPIAR", "CORTAR", "PEGAR", "MAYUS", "FUENTE"};
	JButton green, red, blue, yellow, botones[] = new JButton[buttonTitle.length];
	JFrame ventana;
	int contadorAr;
	Choice font = new Choice();
	FileDialog fd;
	boolean a = false;



	

public Editor(){
	tool2 = new JToolBar("Colors");
	tool2.setLayout(new GridLayout());
	green = new JButton();
	green.setBackground(Color.GREEN);
	green.setFont(letra);
	green.addActionListener(this);
	red = new JButton();
	red.setBackground(Color.RED);
	red.setFont(letra);
	red.addActionListener(this);
	blue = new JButton();
	blue.setBackground(Color.BLUE);
	blue.setFont(letra);
	blue.addActionListener(this);
	yellow = new JButton();
	yellow.setBackground(Color.YELLOW);
	yellow.setFont(letra);
	yellow.addActionListener(this);

	tool2.add(green);
	tool2.add(red);
	tool2.add(blue);
	tool2.add(yellow);
	
	texto = new JTextArea[10];
	contadorAr = 0;
	texto[contadorAr] = new JTextArea();


	pestana = new JTabbedPane();
	//pestana.addTab("Nuevo" ,texto [contadorAr]);
	pestana.addTab("Archivo " + contadorAr, texto[0]);
	menu = new MenuBar();
	archivo = new Menu("Archivo");
	edicion = new Menu("Edicion");
	nuevo = new MenuItem("Nuevo");
	guardar = new MenuItem("Guardar");
	guardar2 = new MenuItem("Guardar como");
	abrir = new MenuItem("Abrir");
	copiar = new MenuItem("Copiar");
	cortar = new MenuItem("Cortar");
	pegar = new MenuItem("Pegar");
	mayus = new MenuItem("Mayuscula");
	minus = new MenuItem("Minuscula");
	tool = new JToolBar("Tools");
	tool.setLayout(new GridLayout());
	for(int i = 0; i < buttonTitle.length; i++){
		botones[i] = new JButton(buttonTitle[i]);
		tool.add(botones[i]);
		botones[i].setFont(letra);
		/*botones[i].setBackground(Color.RED);
		if (i == 0) botones[i].setBackground(Color.GREEN);
		if (i == 2) botones[i].setBackground(Color.BLUE);
		if (i == 3) botones[i].setBackground(Color.YELLOW);*/
		botones[i].addActionListener(this);
		botones[i].setBackground(Color.LIGHT_GRAY);
		botones[i].setLayout(new FlowLayout());
	}
	tool.add(font);
	menu.add(archivo);
	menu.add(edicion);
	archivo.add(nuevo);
	nuevo.addActionListener(this);
	archivo.addSeparator();
	archivo.add(guardar);
	guardar.addActionListener(this);
	archivo.addSeparator();
	archivo.add(guardar2);
	archivo.addSeparator();
	archivo.add(abrir);
	abrir.addActionListener(this);
	edicion.add(copiar);
	edicion.addSeparator();
	edicion.add(pegar);
	edicion.addSeparator();
	edicion.add(cortar);
	edicion.addSeparator();
	edicion.add(mayus);
	edicion.addSeparator();
	edicion.add(minus);
	font.add("Courier");
	font.add("Lucida Calligraphy");
	font.addItemListener(this);
	f.setTitle("Editor");
	f.setMenuBar(menu);
	f.add(pestana, BorderLayout.CENTER);
	f.setSize(800,650);
	f.add(tool, BorderLayout.NORTH);
	f.add(tool2, BorderLayout.SOUTH);
	f.setLocation(400, 100);
	f.setVisible(true);
	f.getContentPane().setBackground(Color.BLACK);
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


public void actionPerformed(ActionEvent e){
	int pos, begin, end;
	String may;
	if(e.getSource().equals(nuevo)){
		if (contadorAr<texto.length){
			contadorAr++;
			texto[contadorAr] = new JTextArea();
			pestana.addTab(" Archivo" + contadorAr, texto[contadorAr]);
		}
	}
	else if(e.getSource() == botones[0]){
		txt = ((JTextArea) pestana.getSelectedComponent()).getSelectedText();
		System.out.println(txt);
}
	else if(e.getSource() == botones[1]){
		begin = ((JTextArea) pestana.getSelectedComponent()).getSelectionStart();
		end = ((JTextArea) pestana.getSelectedComponent()).getSelectionEnd();
		txt = ((JTextArea) pestana.getSelectedComponent()).getSelectedText();
		((JTextArea)pestana.getSelectedComponent()).replaceRange(" ", begin, end);

	}
	
	else if(e.getSource() == botones[2]){
		pos = ((JTextArea) pestana.getSelectedComponent()).getCaretPosition();
		((JTextArea) pestana.getSelectedComponent()).insert(txt, pos);

	}

	else if(e.getSource() == botones[3]){
		may = ((JTextArea) pestana.getSelectedComponent()).getSelectedText().toUpperCase();
		begin = ((JTextArea) pestana.getSelectedComponent()).getSelectionStart();
		end = ((JTextArea) pestana.getSelectedComponent()).getSelectionEnd();
		((JTextArea)pestana.getSelectedComponent()).replaceRange(may, begin, end);

	}

	else if(e.getSource() == botones[4]){
		texto[contadorAr].setFont(letra);
		
	}

	else if(e.getSource().equals(green)){
		texto[contadorAr].setForeground(Color.GREEN);
	}

	else if(e.getSource().equals(red)){
		texto[contadorAr].setForeground(Color.RED);
	}
	else if(e.getSource().equals(blue)){
		texto[contadorAr].setForeground(Color.BLUE);
	}
	else if(e.getSource().equals(yellow)){
		texto[contadorAr].setForeground(Color.YELLOW);
	}

	else if(e.getSource().equals(font + "Courier")){
		texto[contadorAr].setFont(letra2);
	}

	else if(e.getSource().equals(guardar)){
		//String ruta;
		fd = new FileDialog(f, "Guardar", FileDialog.SAVE);
		fd.setVisible(true);
		ruta = fd.getDirectory() + fd.getFile();
		GuardarArchivo();

	}

	else if(e.getSource().equals(abrir)){
		if(contadorAr<texto.length){
			contadorAr++;
			texto[contadorAr] = new JTextArea();
			pestana.addTab("Archivo" + contadorAr, texto[contadorAr]);
		
		fd = new FileDialog(f, "Abrir", FileDialog.LOAD);
		fd.setVisible(true);
		ruta = fd.getDirectory() + fd.getFile();
		AbrirArchivo();
	}
	 	
	 }
}

public void GuardarArchivo(){
	 try
          { 
             File outputFile=new File(ruta);
             FileOutputStream fis=new FileOutputStream(outputFile);
             DataOutputStream dis=new DataOutputStream(fis);
             dis.writeBytes(texto[0].getText());
             dis.close();
             fis.close();
             a=true;
           }
           catch(FileNotFoundException e)
           {
              System.err.println("Error:"+e);
            }
           catch(IOException e)
           {
              System.err.println("Error:"+e);
            }
          }

  public void AbrirArchivo(){
  	String txt;
  	try
          { 
             File inputFile=new File(ruta);
             FileInputStream fis=new FileInputStream(inputFile);
             BufferedReader dis = new BufferedReader(new InputStreamReader(fis));
             while((txt =  dis.readLine()) != null)
             	texto[contadorAr].append(txt + "\n");
             fis.close();
             dis.close();
           }
           catch(FileNotFoundException e)
           {
              System.err.println("Error:"+e);
            }
           catch(IOException e)
           {
              System.err.println("Error:"+e);
            }
          }

  
public void itemStateChanged(ItemEvent e){
	if(e.getSource().equals(font)){
		if(font.getSelectedItem() == "Courier");
		texto[contadorAr].setFont(letra2);
	}

	else if(e.getSource().equals(font)){
		if(font.getSelectedItem() == "Lucida Calligraphy");
		texto[contadorAr].setFont(letra3);
	}
}





public static void main(String[] args){
		new Editor();
	}


}
