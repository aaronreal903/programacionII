import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class VentanaSecundaria implements ActionListener{
	JFrame f2;
	JButton c;
	Eventos prin;

public VentanaSecundaria(Eventos principal){
	prin=principal;
	if(!principal.getBandera()){
	f2 = new JFrame("Ventana secundaria");
	 c = new JButton("Salir");
	 f2.add(c);
	 c.addActionListener(prin);
	 f2.pack();
	 f2.setVisible(true);
	 //f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 prin.setBandera(true);

}
}
public void actionPerformed(ActionEvent e){
	if(e.getSource().equals(c))
     f2.dispose();

}
}
