import java.io.*;
import java.util.*;
class Fichero {
public static void main( String args[] ) {
Scanner ent = new Scanner(System.in);
System.out.println("Ruta, nombre y extension del archivo a comprobar");
File f = new File( ent.nextLine() );
System.out.println( "Nombre: "+f.getName() );
System.out.println( "Camino: "+f.getPath() );
if( f.exists() ) {
System.out.println( "Fichero existente " );
System.out.println( (f.canRead() ? " y se puede Leer" : "No se puede leer" ) );
System.out.println( (f.canWrite() ? " y se puede Escribir" : "No se puede escribir" ) );
System.out.println( "La longitud del fichero son "+ f.length()+" bytes" );
} 
 else
System.out.println( "El fichero no existe" );
}
}