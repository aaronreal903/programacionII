import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class EventosMouse extends MouseAdapter implements MouseMotionListener, ItemListener{

JTextField texto;
JFrame f;
Choice lista;
Checkbox caja;


public EventosMouse(){
	f = new JFrame("MouseEvents");
	texto = new JTextField();
	texto.setBackground(Color.GREEN);
	texto.setForeground(Color.RED);
	caja = new Checkbox("Select here");
	caja.addItemListener(this);
	lista = new Choice();
	lista.add("Azul");
	lista.add("Rojo");
	lista.add("Negro");
	lista.addItemListener(this);
	f.addMouseListener(this);
	f.addMouseMotionListener(this);
	f.add(texto, BorderLayout.SOUTH);
	f.add(lista, BorderLayout.NORTH);
	f.add(caja, BorderLayout.EAST);
	f.setSize(500, 400);
	f.setLocation(400, 100);
	f.setVisible(true);
	f.getContentPane().setBackground(Color.BLACK);
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

}

	public void itemStateChanged(ItemEvent e){
		System.out.println("La seleccion cambio ");
		if(e.getSource().equals(lista))
		System.out.println(lista.getSelectedItem());
	else if(e.getSource().equals(caja))
		System.out.println("Estado " + caja.getState());

	}
	public void mouseEntered(MouseEvent e){
		System.out.println("Dentro de la ventana");
	}

	public void mouseDragged(MouseEvent e){
		System.out.println("Arrastre");
	}

	public void mouseMoved(MouseEvent e){
		texto.setText("x: " + e.getX() + " Y: " + e.getY());

	}

	public static void main(String[] args){
		new EventosMouse();	
	}

}