import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class Conceptos extends JFrame implements ActionListener{
	JFrame f = new JFrame("Conceptos");
	JButton b = new JButton("Siguiente");
	JButton exit = new JButton("Salir");
	JTextArea concepto = new JTextArea();
	//JPanel panel = new JPanel();
	Font fuente = new Font("Arial", Font.PLAIN, 14);

	//JTextArea pregunta = new JTextArea();

	public Conceptos(){
		//f.add(panel);
		f.setVisible(true);
		f.setLayout(new FlowLayout());
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(400, 400);
		f.add(concepto);
		f.add(exit);
		b.addActionListener(this);
		exit.addActionListener(this);
		concepto.setText("Evento \n Suceso que detecta un componente para el cual se puede implementar una respuesta");
		concepto.setEditable(false);
		concepto.setFont(fuente);
		f.add(b);

		
	}

	public void actionPerformed(ActionEvent e){
		if(e.getSource().equals(b)){
			concepto.setText("Que es un evento?");

			//cron = new Cronometro(this);

		}
		else if(e.getSource().equals(exit)){
			f.dispose();
		}
	}
	public static void main(String[]args){
		new Conceptos();
	}
}