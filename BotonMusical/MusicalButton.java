// liga donde pueden obtener sonidos con extension au http://www.ibiblio.org/pub/multimedia/sun-sounds/sound_effects/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;
import java.net.*;

class MusicalButton extends JButton implements ActionListener{

  String tema;
  AudioClip bach;


  public MusicalButton(String text,Icon im,String tem){
     super(text,im);
     tema=tem;
     addActionListener(this);
  }

  public MusicalButton(Icon im,String tem){
     super(im);
     tema=tem;
     addActionListener(this);
  }

  public MusicalButton(String text,String tem){
     super(text);
     tema=tem;
     addActionListener(this);
  }

 public MusicalButton(String tem){
     super();
     tema=tem;
     addActionListener(this);
  }
  
 public void setTema(String tem){
    tema=tem;
 }

 public String getTema(){
    return tema;
}

public void actionPerformed(ActionEvent e){
     try{
        bach = Applet.newAudioClip(new URL(tema));
      }catch (MalformedURLException mfe){
        System.out.println("An error occured, please try again...");
      }
      bach.play();
}
} 

