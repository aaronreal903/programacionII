import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class ObjetoAprendizaje implements ActionListener{
  JFrame ventanaPrincipal, ventanaMenu, Tema1, Tema2, Tema3;
  JButton comienzo, geografia, historia, cnaturales, regreso1, regreso2, regreso3;
  Font fuente =  new Font("Arial", Font.BOLD, 18);

public ObjetoAprendizaje(){
  ventanaPrincipal= new JFrame();
  ventanaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  ventanaPrincipal.setTitle("Objeto de Aprendizaje");
  ventanaPrincipal.setVisible(true);
  ventanaPrincipal.setLocation(400,400);
  ventanaPrincipal.setSize(500, 300);
  JTextArea bienvenida = new JTextArea();
  bienvenida.setText("Bienvenido a nuestro objeto de aprendizaje.\nTrabajaras con los temas de Geografia, Historia y Ciencias Naturales");
  bienvenida.setSize(150,100);
  bienvenida.setEditable(false);
  bienvenida.setFont(fuente);
  ventanaPrincipal.add(bienvenida, BorderLayout.NORTH);
  JLabel instruccion = new JLabel();
  instruccion.setText("Para comenzar haz click sobre el boton");
  instruccion.setFont(fuente);
  ventanaPrincipal.add(instruccion, BorderLayout.CENTER);
  comienzo = new JButton("Iniciar");
  ventanaPrincipal.add(comienzo, BorderLayout.SOUTH);
  //comienzo.setBackground(Color.BLUE);
  comienzo.addActionListener(this);
}

public void SeleccionMenu(){
  ventanaMenu = new JFrame();
  ventanaMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  ventanaMenu.setLayout(new FlowLayout());
  ventanaMenu.setVisible(true);
  ventanaMenu.setLocation(400,400);
  ventanaMenu.setSize(300,300);
  ventanaPrincipal.dispose();
  JLabel eleccion = new JLabel();
  eleccion.setText("Por favor, elige un tema");
  eleccion.setFont(fuente);
  ventanaMenu.add(eleccion);
  geografia = new JButton("Geografia");
  historia = new JButton("Historia");
  cnaturales = new JButton("Ciencias Naturales");
  ventanaMenu.add(geografia);
  ventanaMenu.add(historia);
  ventanaMenu.add(cnaturales);
  geografia.addActionListener(this);
  historia.addActionListener(this);
  cnaturales.addActionListener(this);

}

public void Geografia(){
  Tema1 = new JFrame();
  Tema1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  Tema1.setLayout(new FlowLayout());
  Tema1.setVisible(true);
  Tema1.setLocation(400,400);
  Tema1.setSize(300,300);
  ventanaMenu.dispose();
  JLabel titulo = new JLabel();
  Tema1.add(titulo);
  titulo.setText("GEOGRAFIA");
  titulo.setFont(fuente);
  regreso1 = new JButton("Regresar");
  regreso1.addActionListener(this);
  Tema1.add(regreso1);



}
public void Historia(){
  Tema2 = new JFrame();
  Tema2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  Tema2.setLayout(new FlowLayout());
  Tema2.setVisible(true);
  Tema2.setLocation(400,400);
  Tema2.setSize(300,300);
  ventanaMenu.dispose();
  JLabel titulo2 = new JLabel();
  Tema2.add(titulo2);
  titulo2.setText("HISTORIA");
  titulo2.setFont(fuente);
  regreso2 = new JButton("Regresar");
  regreso2.addActionListener(this);
  Tema2.add(regreso2);

}
public void Ciencias(){
  Tema3 = new JFrame();
  Tema3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  Tema3.setLayout(new FlowLayout());
  Tema3.setVisible(true);
  Tema3.setLocation(400,400);
  Tema3.setSize(300,300);
  ventanaMenu.dispose();
  JLabel titulo3 = new JLabel();
  Tema3.add(titulo3);
  titulo3.setText("Ciencias Naturales");
  titulo3.setFont(fuente);
  regreso3 = new JButton("Regresar");
  regreso3.addActionListener(this);
  Tema3.add(regreso3);
}

public void actionPerformed(ActionEvent e){
  if(e.getSource() == comienzo){
    SeleccionMenu();
  }

  if(e.getSource() == geografia){
    Geografia();
  }
  if(e.getSource() == regreso1){
    SeleccionMenu();
    Tema1.dispose();

  }

  if(e.getSource() == historia){
    Historia();
  }
  if(e.getSource() == regreso2){
    SeleccionMenu();
    Tema2.dispose();
  }
  if(e.getSource() == cnaturales){
    Ciencias();
  }
  if(e.getSource() == regreso3){
    SeleccionMenu();
    Tema3.dispose();
  }
}

  public static void main(String[] args) {
    new ObjetoAprendizaje();
  }

}
